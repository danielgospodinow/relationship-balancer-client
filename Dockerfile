FROM node:12.16.0

WORKDIR /app/

COPY . .

RUN npm install

RUN npm run build

EXPOSE 3000

CMD [ "./node_modules/serve/bin/serve.js", "-s", "build", "-l", "3000" ]
