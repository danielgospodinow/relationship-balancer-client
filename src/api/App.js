import React from 'react';
import logo from '../heart.png';
import './App.css';
import 'typeface-roboto';
import Button from '@material-ui/core/Button';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = { counter: null };
  }

  componentDidMount() {
    fetch('http://localhost:3001/status')
      .then(response => response.json())
      .then(data => this.setState({ counter: data.currentState }));
  }

  increaseCounter() {
    fetch('http://localhost:3001/increase')
      .then(response => response.json())
      .then(data => this.setState({ counter: data.currentState }));
  }

  decreaseCounter() {
    fetch('http://localhost:3001/decrease')
      .then(response => response.json())
      .then(data => this.setState({ counter: data.currentState }));
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div style={{ marginBottom: '70px' }}>
            <img src={logo} className="App-logo" alt="logo" />
          </div>
          <p style={{ marginBottom: '50px' }} >{this.state.counter}</p>
          <div style={{ marginBottom: '10px' }}>
            <Button variant="contained" color="primary" onClick={this.increaseCounter.bind(this)}>Daniel</Button>
          </div>
          <div>
            <Button variant="contained" color="secondary" onClick={this.decreaseCounter.bind(this)}>Boryana</Button>
          </div>
        </header>
      </div>
    );
  }
}

export default App;
